package com.konis.skywars;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

import com.konis.skywars.objects.Player;
import com.konis.skywars.objects.utils.PlayerUtils;

public class Main extends JavaPlugin{
	
	static Main inst;
	
	public Main () {
		inst = this;
	}
	
	@Override
	public void onEnable () {
		inst = this;
	}
	
	
	@Override
	public void onDisable () {
		
	}
	
	@EventHandler
	public void onPlayerJoinEvent(PlayerJoinEvent event) {
	    getServer().getConsoleSender().sendMessage("XD");
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("player")) {
			for (Player p  : PlayerUtils.getPlayerList()) {
				if (args[0].equalsIgnoreCase(p.getName())) {
					sender.sendMessage("Taki gracz istnieje!");
					return true;
				}else {
					sender.sendMessage("Taki gracz nie istnieje!");
					p = new Player (sender.getName());
					return true;
				}
				
			}
			return true;
		}
		return false; 
	}
		
}
