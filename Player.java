package com.konis.skywars.objects;

public class Player {
	String name;
	int points;
	
	public Player (String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	public int getPoints() {
		return points;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	
	
}
