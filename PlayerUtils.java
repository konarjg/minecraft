package com.konis.skywars.objects.utils;

import java.util.ArrayList;
import java.util.List;

import com.konis.skywars.objects.Player;

public class PlayerUtils {
	public static List<Player> playerList = new ArrayList<Player> ();

	public static List<Player> getPlayerList() {
		if (playerList == null) {
			playerList = new ArrayList<Player>();
		}
		return playerList;
	}

	public static void setPlayerList(List<Player> playerListt) {
		playerList = playerListt;
	}
	
	public static void addPlayer(Player p) {
		for (Player x : playerList) {
			if (!p.equals(x)) {
				playerList.add(p);
			}
		}
	}
	
	public static void removePlayer(Player p) {
		for (Player x : playerList) {
			if (p.equals(x)) {
				playerList.remove(p);
			}
		}
	}
	
	
}
